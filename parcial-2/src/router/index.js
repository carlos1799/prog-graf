import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Detalles from '../views/Detalles.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/Detalles',
    name: 'Detalles',
    component: Detalles

  }
]

const router = new VueRouter({
  routes
})

export default router
