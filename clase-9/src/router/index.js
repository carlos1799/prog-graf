import Vue from 'vue'
import VueRouter from 'vue-router'
import Sistema from '../views/Sistema.vue'
import Ventas from '../views/Ventas'
import compras from '../views/compras'
import proovedor from '../views/proovedor'
import colaborador from '../views/colaborador'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'sistema',
    component: Sistema
  },
  {
    path: '/ventas',
    name: 'about',
    component: Ventas
  },
  {
    path: '/compras',
    name: 'compras',
    component: compras
  },
  {
    path: '/proovedor',
    name: 'proovedor',
    component: proovedor
  },
  {
    path: '/colaborador',
    name: 'colaborador',
    component: colaborador
  },
  {
    path: '/venta/:id',
    name: 'producto',
    component: Ventas
  }

]

const router = new VueRouter({
  routes
})

export default router
