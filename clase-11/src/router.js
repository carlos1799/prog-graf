import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Reporte from './views/Reporte.vue'
import Computer from './views/Computer.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/reporte',
      name: 'Reporte',
      component: Reporte
    },
    {
      path: '/computer/:id',
      name: 'Computer',
      component: Computer
    }
  ]
})
